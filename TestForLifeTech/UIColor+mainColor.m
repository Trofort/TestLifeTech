//
//  UIColor+mainColor.m
//  TestForLifeTech
//
//  Created by Maksim Dehanov on 30.04.17.
//  Copyright © 2017 Maksim Dehanov. All rights reserved.
//

#import "UIColor+mainColor.h"

@implementation UIColor (mainColor)

+ (UIColor *)mainColor {
    return [UIColor colorWithRed:149.0/255.0 green:92.0/255.0 blue:255.0/255.0 alpha:1];
}

@end
