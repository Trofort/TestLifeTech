//
//  UIImage+imageFromUrl.h
//  TestForLifeTech
//
//  Created by Maksim Dehanov on 30.04.17.
//  Copyright © 2017 Maksim Dehanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (imageFromUrl)

+ (void)imageFromUrl:(NSURL *)url andCompletion:(void(^)(BOOL success, UIImage *image))completion;

@end
