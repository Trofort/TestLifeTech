//
//  ListOfProductViewController.m
//  TestForLifeTech
//
//  Created by Maksim Dehanov on 28.04.17.
//  Copyright © 2017 Maksim Dehanov. All rights reserved.
//

#import "ListOfProductViewController.h"
#import "ProductCollectionViewCell.h"
#import "NetworkManager.h"
#import "Product.h"
#import "DetailProductViewController.h"
#import "UIImage+imageFromUrl.h"

@interface ListOfProductViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView* collectionView;

@property (strong, nonatomic) NSMutableArray<Product *> *productsArray;
@property (strong, nonatomic) NSCache *cacheImage;

@end

@implementation ListOfProductViewController

#pragma mark - Life cicle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.productsArray = [NSMutableArray new];
    self.cacheImage = [NSCache new];
    [self customize];
    [self getListOfProducts];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.productsArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"productCell" forIndexPath:indexPath];
    [cell setCellWithProduct:self.productsArray[indexPath.row]];
    
    UIImage *cachedImage = [self.cacheImage objectForKey:indexPath];
    
    if (cachedImage) {
        cell.productImageView.image = cachedImage;
    } else {
        [cell.loadIndicator startAnimating];
        [cell.loadIndicator setHidden:NO];
        [UIImage imageFromUrl: self.productsArray[indexPath.row].imageUrl andCompletion:^(BOOL success, UIImage *image) {
            [cell.loadIndicator stopAnimating];
            [cell.loadIndicator setHidden:YES];
            if (success) {
                cell.productImageView.image = image;
                [self.cacheImage setObject:image forKey:indexPath];
            }
        }];
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    DetailProductViewController *detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"detailProductID"];
    [detailVC setViewControllerWithProduct:self.productsArray[indexPath.row]];
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width / 2.0;
    return CGSizeMake(width, width * 1.2f);
}

#pragma mark - Customize 

-(void)customize {
    UINib *productCell = [UINib nibWithNibName:@"ProductCollectionViewCell" bundle:nil];
    [self.collectionView registerNib:productCell forCellWithReuseIdentifier:@"productCell"];
}

#pragma mark - Get List Of Products

-(void)getListOfProducts {
    [[NetworkManager sharedManager] getListOfProductsWithCompletion:^(BOOL success, NSArray *array, NSError *error) {
        if (success) {
            [self.productsArray removeAllObjects];
            for (NSDictionary *dict in array) {
                [self.productsArray addObject:[[Product alloc] initWithDictionary:dict]];
            }
            [self.collectionView reloadData];
        } else {
            UIAlertController *getListAlertController = [UIAlertController alertControllerWithTitle:@"Ошибка" message: error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            [self presentViewController:getListAlertController animated:YES completion:nil];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:nil];
            
            UIAlertAction *repeatAction = [UIAlertAction actionWithTitle:@"Repeat" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self getListOfProducts];
            }];
            
            [getListAlertController addAction:okAction];
            [getListAlertController addAction:repeatAction];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
