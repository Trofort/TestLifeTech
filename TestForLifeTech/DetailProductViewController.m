//
//  DetailProductViewController.m
//  TestForLifeTech
//
//  Created by Maksim Dehanov on 29.04.17.
//  Copyright © 2017 Maksim Dehanov. All rights reserved.
//

#import "DetailProductViewController.h"
#import "NetworkManager.h"
#import "Product.h"
#import "UIImage+imageFromUrl.h"
@interface DetailProductViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadIndicator;

@property (strong, nonatomic) Product *localProduct;

@end

@implementation DetailProductViewController

#pragma mark - Life cicle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customize];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - setLocalProduct

-(void)setViewControllerWithProduct:(Product *)product {
    self.localProduct = product;
}

#pragma mark - Customize

-(void)customize {
    [[NetworkManager sharedManager] getProductDescriptionById:self.localProduct.productId andCompletion:^(BOOL success, NSDictionary *dict, NSError *error) {
        if (success) {
            if (self.localProduct) {
                [self.localProduct changeWithDictionary:dict];
                [self.loadIndicator startAnimating];
                [UIImage imageFromUrl:self.localProduct.imageUrl andCompletion:^(BOOL success, UIImage *image) {
                    [self.loadIndicator stopAnimating];
                    [self.loadIndicator setHidden:YES];
                    if (success) {
                        self.productImageView.image = image;
                    }
                }];
                self.productTitleLabel.text = self.localProduct.name;
                self.productDescriptionLabel.text = self.localProduct.productDescription;
                self.productPriceLabel.text = [NSString stringWithFormat:@"%@$", self.localProduct.price];
            }
        } else {
            UIAlertController *getProductAlertController = [UIAlertController alertControllerWithTitle:@"Ошибка" message: error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            [self presentViewController:getProductAlertController animated:YES completion:nil];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:nil];
            
            UIAlertAction *repeatAction = [UIAlertAction actionWithTitle:@"Repeat" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self customize];
            }];
            
            [getProductAlertController addAction:okAction];
            [getProductAlertController addAction:repeatAction];
        }
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
