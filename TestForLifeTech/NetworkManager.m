//
//  NetworkManager.m
//  TestForLifeTech
//
//  Created by Maksim Dehanov on 28.04.17.
//  Copyright © 2017 Maksim Dehanov. All rights reserved.
//

#import "NetworkManager.h"

@interface NetworkManager ()

@end
static NSString *url = @"https://s3-eu-west-1.amazonaws.com/developer-application-test/";

@implementation NetworkManager

+ (NetworkManager *)sharedManager {
        static NetworkManager *sharedNetworkManager = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sharedNetworkManager = [[NetworkManager alloc] init];
        });
        return sharedNetworkManager;
}

-(void)getListOfProductsWithCompletion:(void(^)(BOOL success, NSArray *array, NSError *error))completion {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[url stringByAppendingString:@"cart/list"]]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(YES, [json objectForKey:@"products"], nil);
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(NO, nil, error);
            });
        }
    }] resume];
}

-(void)getProductDescriptionById:(NSNumber *)productId andCompletion:(void(^)(BOOL success, NSDictionary *dict, NSError *error))completion {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@cart/%@/detail", url, productId]]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(YES, json, nil);
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(NO, nil, error);
            });
        }
    }] resume];
}

@end
