//
//  NetworkManager.h
//  TestForLifeTech
//
//  Created by Maksim Dehanov on 28.04.17.
//  Copyright © 2017 Maksim Dehanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject

+(NetworkManager *)sharedManager;

-(void)getListOfProductsWithCompletion:(void(^)(BOOL success, NSArray *array, NSError *error))completion;
-(void)getProductDescriptionById:(NSNumber *)productId andCompletion:(void(^)(BOOL success, NSDictionary *dict, NSError *error))completion;

@end
