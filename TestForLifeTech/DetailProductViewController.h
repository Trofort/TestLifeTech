//
//  DetailProductViewController.h
//  TestForLifeTech
//
//  Created by Maksim Dehanov on 29.04.17.
//  Copyright © 2017 Maksim Dehanov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@interface DetailProductViewController : UIViewController

-(void)setViewControllerWithProduct:(Product *)product;

@end
