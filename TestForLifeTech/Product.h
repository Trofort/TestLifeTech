//
//  Product.h
//  TestForLifeTech
//
//  Created by Maksim Dehanov on 28.04.17.
//  Copyright © 2017 Maksim Dehanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (strong, nonatomic) NSNumber *productId;
@property (strong, nonatomic) NSURL *imageUrl;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSNumber *price;
@property (strong, nonatomic) NSString *productDescription;

-(Product *)initWithDictionary:(NSDictionary *)dict;

-(void)changeWithDictionary:(NSDictionary *)dict;

@end
