//
//  Product.m
//  TestForLifeTech
//
//  Created by Maksim Dehanov on 28.04.17.
//  Copyright © 2017 Maksim Dehanov. All rights reserved.
//

#import "Product.h"

@implementation Product

-(Product *)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.productId = [dict objectForKey:@"product_id"];
        self.imageUrl = [NSURL URLWithString:[dict objectForKey:@"image"]];
        self.name = [dict objectForKey:@"name"];
        self.price = [dict objectForKey:@"price"];
    }
    return self;
}

- (void)changeWithDictionary:(NSDictionary *)dict {
    self.productId = [dict objectForKey:@"product_id"];
    self.imageUrl = [NSURL URLWithString:[dict objectForKey:@"image"]];
    self.name = [dict objectForKey:@"name"];
    self.price = [dict objectForKey:@"price"];
    self.productDescription = [dict objectForKey:@"description"];
}

@end
