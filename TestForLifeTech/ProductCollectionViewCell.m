//
//  ProductCollectionViewCell.m
//  TestForLifeTech
//
//  Created by Maksim Dehanov on 28.04.17.
//  Copyright © 2017 Maksim Dehanov. All rights reserved.
//

#import "ProductCollectionViewCell.h"
#import "UIColor+mainColor.h"

@interface ProductCollectionViewCell()


@end

@implementation ProductCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [UIColor mainColor].CGColor;
}

-(void)setCellWithProduct:(Product *)product {
    self.productTitleLabel.text = product.name;
    self.productPriceLabel.text = [NSString stringWithFormat:@"%@$", product.price];
}

@end
