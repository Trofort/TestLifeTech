//
//  UIColor+mainColor.h
//  TestForLifeTech
//
//  Created by Maksim Dehanov on 30.04.17.
//  Copyright © 2017 Maksim Dehanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (mainColor)

+(UIColor *)mainColor;

@end
