//
//  UIImage+imageFromUrl.m
//  TestForLifeTech
//
//  Created by Maksim Dehanov on 30.04.17.
//  Copyright © 2017 Maksim Dehanov. All rights reserved.
//

#import "UIImage+imageFromUrl.h"

@implementation UIImage (imageFromUrl)

+ (void)imageFromUrl:(NSURL *)url andCompletion:(void(^)(BOOL success, UIImage *image))completion {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        NSData *dataImage = [NSData dataWithContentsOfURL:url];
        
        UIImage* image = [[UIImage alloc] initWithData:dataImage];
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(YES, image);
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(NO, nil);
            });
        }
    });
}

@end
