//
//  ProductCollectionViewCell.h
//  TestForLifeTech
//
//  Created by Maksim Dehanov on 28.04.17.
//  Copyright © 2017 Maksim Dehanov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@interface ProductCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadIndicator;

-(void)setCellWithProduct:(Product *)product;

@end
